// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyAkxK0e2vZFVQ1l3DBIzrJYAkClox_8HcA",
    authDomain: "angularhome-baea1.firebaseapp.com",
    databaseURL: "https://angularhome-baea1.firebaseio.com",
    projectId: "angularhome-baea1",
    storageBucket: "angularhome-baea1.appspot.com",
    messagingSenderId: "639842867394",
    appId: "1:639842867394:web:1791539c3028d3b624435b",
    measurementId: "G-J4JW1S6EFJ"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
