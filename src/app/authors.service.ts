import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthorsService {

  listOfAuthors:any = [{id:1, title:'Alice in Wonderland', author:'Lewis Carrol'},{id:2, title:'War and Peace', author:'Leo Tolstoy'}, {id:3, title:'The Magic Mountain', author:'Thomas Mann'}, {id:4, title:'Michael', author:'Amos Oz'}] 


  getAuthors(){
    const authorsObservable = new Observable(
      observer => {
        setInterval(
          () => observer.next(this.listOfAuthors),4000
        )
      }
    )
    return authorsObservable;
  }

  addAuthors(){
    setInterval(
      () => this.listOfAuthors.push({id:100, title:'A new book', author:'New author'})
      ,4000)
  }
  constructor() { }
}
