import { Component, OnInit, } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AuthorsService } from './../authors.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-authors',
  templateUrl: './authors.component.html',
  styleUrls: ['./authors.component.css']
})
export class AuthorsComponent implements OnInit {

 AuthorsList: any;
 constructor(private route: ActivatedRoute, private authorsservice:AuthorsService) { }
 
 author;
 id;
 
 listOfAuthors: any;  
 listOfAuthors$:Observable<any>;

 ngOnInit() {
   /*
   this.author = this.route.snapshot.params.author;
   this.id = this.route.snapshot.params.id;
   */
  this.authorsservice.addAuthors();
  this.listOfAuthors$ = this.authorsservice.getAuthors(); 
  }

}
